import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import GestureRecognizer from 'react-native-swipe-gestures';
import { Header } from '../components/Header';

const config = {
    velocityThreshold: 0.3,
    directionalOffsetThreshold: 80,
};

export const ModalGallery = ({ isModalAlbomVisible, setIsModalAlbomVisible, currentAlbom, swipeRight, swipeLeft }) => {
    // const onSwipeLeft = () => {
    //     swipeLeft();
    // };

    // const onSwipeRight = () => {
    //     swipeRight();
    // };
    return (
        <GestureRecognizer
            onSwipeLeft={swipeLeft}
            onSwipeRight={swipeRight}
            config={config}
            style={[
                styles.modalStyle,
                {
                    position: 'absolute',
                    zIndex: isModalAlbomVisible ? 5 : -5,
                    width: isModalAlbomVisible ? '100%' : 0,
                    height: isModalAlbomVisible ? '100%' : 0,
                    opacity: isModalAlbomVisible ? 1 : 0,
                },
            ]}
        >
            <View style={styles.modalBox}>
                <Header
                    style={[styles.root, { backgroundColor: isModalAlbomVisible ? 'white' : 'transparency' }]}
                    setIsModalContactVisible={setIsModalAlbomVisible}
                />
                <View style={styles.textBox}>
                    <Image style={styles.AlbomImage} source={{ uri: currentAlbom.imageAlbom }} />
                    <Text style={styles.AlbomNameText}>{currentAlbom.titleAlbom}</Text>
                </View>
            </View>
        </GestureRecognizer>
    );
};

const styles = StyleSheet.create({
    modalStyle: {},
    modalBox: {
        flex: 1,
        backgroundColor: 'grey',
        opacity: 0.9,
        paddingHorizontal: 25,
    },
    AlbomNameText: {
        fontSize: 20,
        fontWeight: '400',
        color: 'white',
    },
    AlbomImage: {
        width: 310,
        height: 115,
        borderRadius: 5,
    },
    textBox: {
        height: '70%',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
