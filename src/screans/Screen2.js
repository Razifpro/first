import React, { useState, useEffect, useRef } from 'react';
import {
    StyleSheet,
    ScrollView as TouchableWithoutFeedback,
    TextInput,
    TouchableOpacity,
    View,
    Text,
    Keyboard,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as Animatable from 'react-native-animatable';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useKeyboard, useDimensions } from '@react-native-community/hooks';
import { Header } from '../components/Header';

const secureObject = {
    login: 'admin',
    password: 'admin',
};
const Screen2 = () => {
    const keyboard = useKeyboard();
    const { height } = useDimensions().window;
    const animateRef = useRef(null);
    const [valueLogin, setValueLogin] = useState('');
    const [valuePassword, setValuePassword] = useState('');
    const navigation = useNavigation();
    const [animationTrigger, setAnimationTrigger] = useState(false);

    // console.log('keyboard isKeyboardShow::: ', keyboard.keyboardShown)
    // console.log('keyboard isKeyboardHeight::: ', keyboard.keyboardHeight)

    //console.log('widtht::: ', width)
    // console.log('hidtht::: ', height)

    useEffect(() => {
        animateRef.current.animate('rotate', 500);
    }, [animationTrigger]);

    const saveToStorage = async value => {
        try {
            await AsyncStorage.setItem('AUTORIZED', value);
        } catch (error) {
            console.log(error);
        }
    };

    const handleSubmit = () => {
        if (valueLogin.toLowerCase() !== secureObject.login || valuePassword.toLowerCase() !== secureObject.password) {
            //npm install @react-native-async-storage/async-storagealertHandler('Please, enter correct credentials')
            setAnimationTrigger(!animationTrigger);
        }
        if (valueLogin.toLowerCase() === secureObject.login && valuePassword.toLowerCase() === secureObject.password) {
            saveToStorage('true');
            //alertHandler('Correct')
            setTimeout(() => navigation.navigate('Screen 3'), 1000);
        }
        return null;
    };

    // const alertHandler = (x) => Alert.alert(x, { cancelable: false })

    return (
        <View style={styles.root}>
            <Header title="Log in" />
            <View
                style={[
                    styles.boxStyle,
                    { height: keyboard.keyboardShown ? height - 50 - keyboard.keyboardHeight : height - 50 },
                ]}
            >
                <TouchableWithoutFeedback style={{ flex: 1 }} onPress={Keyboard.dismiss}>
                    <View style={[styles.scrollContainer, { paddingTop: keyboard.keyboardShown ? 25 : 150 }]}>
                        <Animatable.View style={{ width: '90%' }} ref={animateRef}>
                            <TextInput
                                style={styles.inputStyle}
                                placeholder="Enter your Login"
                                value={valueLogin}
                                onChangeText={setValueLogin}
                            />
                            <TextInput
                                style={styles.inputStyle}
                                placeholder="Enter your Password"
                                value={valuePassword}
                                onChangeText={setValuePassword}
                                onEndEditing={() => Keyboard.dismiss()}
                                onBlur={() => Keyboard.dismiss()}
                            />
                        </Animatable.View>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableOpacity style={styles.buttonStyle} onPress={handleSubmit}>
                    <Text style={styles.textStyle}>Submit</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default Screen2;

const styles = StyleSheet.create({
    root: {
        flex: 1,
    },
    boxStyles: {},
    scrollStyle: {
        //flex: 1
    },
    scrollContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 25,
    },
    inputStyle: {
        width: 300,
        height: 55,
        margin: 12,
        borderWidth: 1,
        borderColor: '#E5E5E5',
        paddingLeft: 30,
        borderRadius: 20,
        fontSize: 18,
    },
    buttonStyle: {
        marginBottom: 10,
        borderRadius: 20,
        width: 300,
        height: 60,
        alignSelf: 'center',
        //marginTop: 150,
        bottom: 40,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#00ADD3',
    },
    textStyle: {
        fontSize: 16,
        color: '#FFFFFF',
    },
});
