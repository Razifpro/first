import React, { useEffect, useState } from 'react';
import { StyleSheet, View, ActivityIndicator, Alert } from 'react-native';
import { Header } from '../components/Header';
import { Screen1 } from './Screen1';
import { ModalScreen } from './ModalScreen';

const url = 'https://randomuser.me/api/?results=50';

const Screen0 = () => {
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);
    const [refresh, setRefresh] = useState(false);

    const [isModalVisible, setModalVisible] = useState(false);
    const [pickerValue, pickerValueChange] = useState(1);

    useEffect(() => {
        asyncHadler();
    }, [refresh]);

    const pickerHandler = item => {
        pickerValueChange(item);
        setTimeout(() => setModalVisible(!isModalVisible), 1000);
    };

    const asyncHadler = async () => {
        try {
            const response = await fetch(url);
            const users = await response.json();

            setData(users.results);
            setLoading(false);
        } catch (error) {
            setLoading(false);
            alertHandler(error);
        }
    };

    const alertHandler = error =>
        Alert.alert(
            `${error}`,
            'Repeat the reguest?',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel'),
                    style: 'Cancel',
                },
                { text: 'Ok', onPress: () => setRefresh(!refresh) },
            ],
            { cancellable: false },
        );

    if (isLoading) {
        return <ActivityIndicator style={styles.indicatorStyle} size="large" color="black" />;
    }

    return (
        <View style={styles.root}>
            <ModalScreen isModalVisible={isModalVisible} pickerHandler={pickerHandler} pickerValue={pickerValue} />
            <Header title="Contacts" setIsVisible={() => setModalVisible(!isModalVisible)} pickerValue={pickerValue} />

            <Screen1 data={data} />
        </View>
    );
};

export default Screen0;

const styles = StyleSheet.create({
    root: {
        flex: 1,
    },
    indicatorStyle: {
        flex: 1,
        justifyContent: 'center',
    },
});
