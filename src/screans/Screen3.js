import React, { useEffect, useState } from 'react';
import { StyleSheet, FlatList, View, Alert, ActivityIndicator } from 'react-native';
import { Header } from '../components/Header';
import { ModalScreen } from './ModalScreen';
import { ModalGallery } from './ModalGallery';
import { Gallery } from '../components/Gallery';

const urlAlbom = 'https://jsonplaceholder.typicode.com/photos?_limit=10&page=';

const Screen3 = () => {
    const [dataAlbom, setDataAlbom] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const [refreshAlbom, setRefreshAlbom] = useState(false);
    const [isModalVisible, setModalVisible] = useState(false);
    const [pickerValue, pickerValueChange] = useState(1);

    const [isModalAlbomVisible, setIsModalAlbomVisible] = useState(false);
    const [currentAlbom, setCurrentAlbom] = useState({});
    const [userIndex, setUserIndex] = useState(null);

    const [page, setPage] = useState(1);
    const [updatedData, setUpdatedData] = useState([]);

    const swipeLeft = () => {
        setUserIndex(userIndex + 1);
    };
    const swipeRight = () => {
        userIndex - 1 < 0 ? Alert.alert("It's a Beginning") : setUserIndex(userIndex - 1);
    };
    const pickerHandler = (item, array) => {
        pickerValueChange(item);
        const filteredData = array.filter(element => element.albumId === pickerValue);
        setUpdatedData(filteredData);
        setTimeout(() => setModalVisible(!isModalVisible), 500);
        console.log('PickerValue::::', pickerValue);
    };

    useEffect(() => {
        const albom = updatedData[userIndex];
        const galleryData = {
            titleAlbom: albom?.title,
            imageAlbom: albom?.url,
        };
        setCurrentAlbom(galleryData);
    }, [userIndex, updatedData]);

    useEffect(() => {
        getAlbom();
    }, []);

    const getAlbom = async () => {
        try {
            const response = await fetch(urlAlbom + page);
            const alboms = await response.json();
            console.log('::: alboms = ', alboms);
            setDataAlbom(alboms);
            setLoading(false);
        } catch (error) {
            alertHandler(error, setRefreshAlbom, refreshAlbom);
        }
    };

    useEffect(() => {
        if (page === 1) {
            setUpdatedData(dataAlbom);
        } else {
            setUpdatedData(updatedData.concat(dataAlbom));
        }
        setUpdatedData(updatedData.concat(dataAlbom));
    }, [dataAlbom, page]);

    const alertHandler = (error, message, func, param) =>
        Alert.alert(
            `${error}`,
            message,
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel'),
                    style: 'Cancel',
                },
                { text: 'Ok', onPress: () => func(!param) },
            ],
            { cancellable: false },
        );

    if (isLoading) {
        return <ActivityIndicator style={styles.indicatorStyle} size="large" color="black" />;
    }

    const setPageHandler = () => {
        if (page < 5) {
            setPage(page + 1);
        } else {
            null;
        }
    };

    return (
        <>
            <ModalGallery
                isModalAlbomVisible={isModalAlbomVisible}
                setIsModalAlbomVisible={() => setIsModalAlbomVisible(!isModalAlbomVisible)}
                currentAlbom={currentAlbom}
                swipeLeft={swipeLeft}
                swipeRight={swipeRight}
            />
            <View style={styles.fon}>
                <ModalScreen
                    isModalVisible={isModalVisible}
                    pickerHandler={pickerHandler}
                    pickerValue={pickerValue}
                    updatedData={updatedData}
                />
                <Header
                    title="Gallery"
                    setIsVisible={() => setModalVisible(!isModalVisible)}
                    pickerValue={pickerValue}
                    isModalAlbomVisible={isModalAlbomVisible}
                />
                <FlatList
                    contentContainerStyle={styles.scrollContainer}
                    data={updatedData}
                    extraData={updatedData}
                    renderItem={({ item, index }) => (
                        <Gallery
                            title={item.title}
                            foto={{ uri: item.url }}
                            albomNomer={item.albumId}
                            setIsModalAlbomVisible={() => setIsModalAlbomVisible(!isModalAlbomVisible)}
                            setCurrentUserIndex={() => setUserIndex(index)}
                        />
                    )}
                    keyExtractor={(item, index) => index}
                    onEndReached={() => setPageHandler()}
                    onEndReachedThreshold={0.1}
                />
            </View>
        </>
    );
};
export default Screen3;

const styles = StyleSheet.create({
    fon: {
        flex: 1,
        backgroundColor: '#0A0A0A',
        alignItems: 'center',
        justifyContent: 'center',
    },

    styleIco: {
        width: 310,
        height: 118,
        borderRadius: 5,
    },
    styleText: {
        fontSize: 12,
        color: '#FFFFFF',
        alignItems: 'flex-end',
        paddingTop: 10,
    },
    titleBox: {
        alignItems: 'center',
        flexDirection: 'row',
    },
    scrollContainer: {
        paddingTop: 70,
    },
    textalbomStyle: {
        fontSize: 15,
        fontWeight: '100',
        color: '#00ADD3',
        flex: 2,
        paddingTop: 10,
        left: '250%',
    },
    numberIcon: {
        width: 28,
        height: 28,
        borderRadius: 14,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#00ADD3',
        marginRight: '4%',
        marginTop: 10,
    },
    albomNomerStyle: {
        color: '#FFFFFF',
        fontSize: 17,
    },
    indicatorStyle: {
        flex: 1,
        justifyContent: 'center',
    },
});
