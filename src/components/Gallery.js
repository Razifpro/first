import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text, Image } from 'react-native';

export const Gallery = ({ title, foto, albomNomer, setIsModalAlbomVisible, setCurrentUserIndex }) => {
    const onModalHandle = () => {
        setIsModalAlbomVisible();
        setCurrentUserIndex();
        console.log('нажатие', onModalHandle);
    };
    return (
        <View style={styles.imageBox}>
            <TouchableOpacity style={styles.styleIco} onPress={onModalHandle}>
                <View>
                    <Image style={styles.styleIco} source={foto} />
                </View>
            </TouchableOpacity>
            <View style={styles.titleBox}>
                <Text style={styles.styleText}>{title}</Text>
                <Text style={styles.textalbomStyle}>ALBUM</Text>
                <View style={styles.numberIcon}>
                    <Text style={styles.albomNomerStyle}>{albomNomer}</Text>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    imageBox: {
        width: 330,
        height: 180,
        borderRadius: 15,
        paddingLeft: 8,
        justifyContent: 'center',
        borderColor: '#444444',
        borderWidth: 2,
        marginBottom: 20,
    },
    styleIco: {
        width: 310,
        height: 118,
        borderRadius: 5,
    },
    styleText: {
        flex: 8,
        fontSize: 12,
        color: '#FFFFFF',
        alignItems: 'flex-end',
        paddingTop: 10,
    },
    titleBox: {
        alignItems: 'center',
        flexDirection: 'row',
    },
    textalbomStyle: {
        fontSize: 15,
        fontWeight: '100',
        color: '#00ADD3',
        flex: 2,
        paddingTop: 10,
        // left: '250%'
    },
    numberIcon: {
        width: 28,
        height: 28,
        borderRadius: 14,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#00ADD3',
        marginRight: '4%',
        marginTop: 10,
    },
    albomNomerStyle: {
        color: '#FFFFFF',
        fontSize: 17,
    },
});
