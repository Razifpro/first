import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { ButtonCircle } from './ButtonCircle';

export const Header = ({ title, setIsVisible, pickerValue, setIsModalContactVisible: setIsModalAlbomVisible }) => {
    const navigation = useNavigation();

    const navigateHandle = () => {
        setIsModalAlbomVisible ? setIsModalAlbomVisible() : navigation.goBack();
    };

    return (
        <View style={[styles.root, { backgroundColor: title !== 'Gallery' ? 'white' : 'black' }]}>
            {title !== 'Contacts' && <ButtonCircle onPress={navigateHandle} />}
            <Text style={[styles.headerText, { color: title === 'Gallery' ? 'white' : 'black' }]}>{title}</Text>
            <View style={[styles.modalBox, { display: title === 'Gallery' ? 'flex' : 'none' }]}>
                <TouchableOpacity style={styles.textStyle} onPress={setIsVisible}>
                    <Text style={styles.textSellect}>Select album</Text>
                </TouchableOpacity>
                <View style={styles.iconSellect}>
                    <Text style={styles.pickerStyle}>{pickerValue}</Text>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    headerText: {
        fontSize: 36,
        flexDirection: 'row',
    },
    root: {
        paddingTop: 10,
        flexDirection: 'row',
        paddingBottom: 10,
        paddingHorizontal: 30,
    },
    textSellect: {
        color: '#00ADD3',
        fontSize: 20,
    },
    modalBox: {
        display: 'none',
        borderWidth: 2,
        flexDirection: 'row',
        alignItems: 'center',
        left: 20,
    },
    iconSellect: {
        width: 30,
        height: 30,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#00ADD3',
        left: '20%',
    },
    pickerStyle: {
        fontSize: 25,
        //fontWeight: '100',
        color: 'white',
    },
});
