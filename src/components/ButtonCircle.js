import React from 'react'
import { StyleSheet, Image, TouchableOpacity } from 'react-native'
import arrowIcon from '../assets/images/arrow.png'

export const ButtonCircle = ({ onPress }) => (
  <TouchableOpacity style={styles.buttonCircle} onPress={() => onPress(1)}>
    <Image style={styles.iconStyle} source={arrowIcon} />
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: '#00ADD3',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 20
  },
  iconStyle: {
    height: 20,
    width: 15
  }
})
