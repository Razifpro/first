import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, Platform, UIManager, LayoutAnimation, TouchableOpacity } from 'react-native';

if (Platform.OS === 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}

const AddresBlock = ({ title, pol }) => {
    return (
        <>
            <View style={styles.addresBox}>
                <View styles={styles.infoBox}>
                    <Text style={styles.infoTittle}>{title}</Text>
                    <Text style={styles.infoContent}>{pol}</Text>
                </View>
            </View>
        </>
    );
};

export const UserCard = ({ name, phone, userPic, userAddress, userMail }) => {
    const [isOpen, setIsOpen] = useState(false);

    const handlePress = () => {
        setIsOpen(!isOpen);
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    };

    return (
        <>
            <TouchableOpacity style={[styles.cardStyle, { height: isOpen ? 190 : 100 }]} onPress={handlePress}>
                <View style={styles.mainBox}>
                    <View style={styles.styleIco}>
                        <Image style={styles.iconStyle} source={userPic} />
                    </View>

                    <View style={styles.titleBox}>
                        <Text style={styles.nameStyle}>{name}</Text>
                        <Text style={styles.phoneStyle}>{phone}</Text>
                    </View>
                </View>
                {isOpen && (
                    <View style={styles.hiddenBox}>
                        <AddresBlock title="ADDRES" pol={userAddress} />
                        <AddresBlock title="EMAIL" pol={userMail} />
                    </View>
                )}
            </TouchableOpacity>
        </>
    );
};

const styles = StyleSheet.create({
    nameStyle: {
        fontSize: 25,
        fontWeight: '900',
        color: '#00ADD3',
    },
    cardStyle: {
        width: 363,
        height: 190,
        backgroundColor: '#E5E5E5',
        borderRadius: 10,
        marginBottom: 10,
    },
    styleIco: {
        width: 60,
        height: 60,
        backgroundColor: '#00ADD3',
        borderRadius: 30,
    },

    mainBox: {
        flexDirection: 'row',
        paddingTop: 20,
        paddingLeft: 30,
    },
    titleBox: {
        flex: 6,
        paddingLeft: 25,
        justifyContent: 'center',
    },
    phoneStyle: {
        fontSize: 15,
        fontWeight: '200',
        color: '#00ADD3',
    },
    hiddenBox: {
        paddingLeft: 115,
        width: 300,
    },
    infoTittle: {
        fontSize: 12,
        fontWeight: '800',
        color: '#00ADD3',
    },
    infoContent: {
        fontSize: 12,
        fontWeight: '200',
        color: '#0A0A0A',
    },

    iconStyle: {
        width: 60,
        height: 60,
        borderWidth: 5,
        backgroundColor: '#00ADD3',
        borderRadius: 30,
    },
    addresBox: {
        paddingTop: 10,
        flexDirection: 'row',
    },

    infoBox: {
        flex: 6,
    },
    scrollStyle: {
        flex: 1,
    },
    scrollContainer: {
        paddingTop: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
