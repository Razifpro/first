export const pickerArray = [
    {
        title: 'Album One',
        value: 1,
    },
    {
        title: 'Album Two',
        value: 2,
    },
    {
        title: 'Album Three',
        value: 3,
    },
    {
        title: 'Album Four',
        value: 4,
    },
];
