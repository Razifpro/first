import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import Screen0 from '../screans/Screen0';
import Screen2 from '../screans/Screen2';
import Screen3 from '../screans/Screen3';

import { TapBar } from '../components/TapBar';

const RootStack = createStackNavigator();
const SecureStack = createStackNavigator();
const Tabs = createBottomTabNavigator();

const SecureScreens = () => (
    <SecureStack.Navigator initialRouteName="Screen 2" headerMode="none">
        <SecureStack.Screen name="Screen 2" component={Screen2} />
        <SecureStack.Screen name="Screen 3" component={Screen3} />
    </SecureStack.Navigator>
);

const TabScreens = () => (
    <Tabs.Navigator
        headerMode="none"
        initialRouteName="Screen0"
        tabBar={({ navigation, state, descriptors }) => <TapBar {...{ navigation, state, descriptors }} />}
    >
        <Tabs.Screen name="Screen0" component={Screen0} />
        <Tabs.Screen name="Screen 2" component={SecureScreens} />
    </Tabs.Navigator>
);

const Navigation = () => (
    <NavigationContainer>
        <RootStack.Navigator initialRouteName="TabScreens" headerMode="none">
            <RootStack.Screen name="TabScreens" component={TabScreens} />
        </RootStack.Navigator>
    </NavigationContainer>
);

export default Navigation;
